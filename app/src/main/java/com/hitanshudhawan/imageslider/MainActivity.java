package com.hitanshudhawan.imageslider;

import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private List<Integer> images;
    private ViewPagerAdapter viewPagerAdapter;

    private LinearLayout dotsLayout;
    private List<ImageView> dots;
    private Integer dotsCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.view_pager);

        images = new ArrayList<>();
        images.add(R.drawable.n1);
        images.add(R.drawable.n2);
        images.add(R.drawable.n3);
        images.add(R.drawable.n4);
        images.add(R.drawable.n5);

        viewPagerAdapter = new ViewPagerAdapter(MainActivity.this, images);
        viewPager.setAdapter(viewPagerAdapter);

        dotsLayout = (LinearLayout) findViewById(R.id.dots_layout);
        dotsCount = viewPagerAdapter.getCount();
        dots = new ArrayList<>(dotsCount);
        for (int i = 0; i < dotsCount; i++) {
            dots.add(i, new ImageView(MainActivity.this));
            dots.get(i).setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.inactive_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 0, 8, 0);

            dotsLayout.addView(dots.get(i), params);
        }
        dots.get(0).setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.active_dot));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotsCount; i++) {
                    dots.get(i).setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.inactive_dot));
                }
                dots.get(position).setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.active_dot));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
